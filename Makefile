doc-html: all
	[ ! -d "./doc" ] && mkdir doc
	@./clictest man -verbosity 3 -format html > doc/clictest.html

all:
	dune build ./clictest.exe
	cp _build/default/clictest.exe clictest
