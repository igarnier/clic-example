open Tezos_clic
open Tezos_error_monad
open Error_monad


let time_group =
  {
    Clic.name = "time_group";
    title = "Time-related commands";
  }

module Display_time_cmd = struct

  let display_time_handler :
        bool * float option ->
        unit -> unit Tezos_error_monad.Error_monad.tzresult Lwt.t =
    fun (minutes, offset_opt) () ->
    let t = Unix.gettimeofday () in
    let offset = Option.value ~default:0.0 offset_opt in
    (if minutes then
       Format.printf "Current time: %f minutes since the epoch@." ((t +. offset) /. 60.)
     else
       Format.printf "Current time: %f seconds since the epoch@." (t +. offset)) ;
    return_unit

  let options =
    Clic.args2
      (Clic.switch
         ~doc:"Display time in minutes rather than seconds"
         ~short:'m'
         ~long:"minutes"
         ())
      (Clic.arg
         ~doc:"Adds given offset to current time"
         ~long:"offset"
         ~placeholder:"float"
         (Clic.parameter (fun (_ctxt : unit) parsed -> return (float_of_string parsed))))

  let params = Clic.(prefixes ["display"; "time"; "since"; "epoch"] stop)

  let command =
    Clic.command
      ~desc:"Display current time"
      ~group:time_group
      options
      params
      display_time_handler

end

module Compute_square_cmd = struct

  let handler : unit ->
                string ->
                string -> unit -> unit Tezos_error_monad.Error_monad.tzresult Lwt.t =
    fun () value exponent () ->
    match
      let v = float_of_string value in
      let e = float_of_string exponent in
      v ** e
    with
    | exception (Failure _) ->
       failwith "Error encountered when computing exponent. Badly formatted input?"
    | result ->
       (Format.printf "%f" result ;
        return_unit)

  let options =
    Clic.no_options

  let params = Clic.(prefix "compute" @@
                     string
                       ~name:"FLOAT"
                       ~desc:"Value we want to compute the power for" @@
                       prefixes ["to"; "the"; "power"; "of"] @@
                     string
                       ~name:"FLOAT"
                       ~desc:"Exponent" @@
                       stop)

  let command =
    Clic.command
      ~desc:"Compute power of a number"
      ~group:time_group
      options
      params
      handler

end

module Global_options = struct

  let say_hello =
    Clic.switch
      ~doc:"Says hello"
      ~long:"say-hello"
      ()

  let say_something =
    Clic.switch
      ~doc:"Says something"
      ~long:"say-something"
      ()

  let options = Clic.args2 say_hello say_something
end

let commands_with_man =
  Clic.add_manual
    ~executable_name:(Filename.basename Sys.executable_name)
    ~global_options:Global_options.options
    (if Unix.isatty Unix.stdout then Clic.Ansi else Clic.Plain)
    Format.std_formatter
    [ Display_time_cmd.command;
      Compute_square_cmd.command; ]

let usage () =
  Clic.usage
    Format.std_formatter
    ~executable_name:(Filename.basename Sys.executable_name)
    ~global_options:Global_options.options
    commands_with_man

let handle_global_options say_hello say_something =
  (if say_hello then
     Format.printf "hello@."
   else () );
  if say_something then
     Format.printf "something@."
   else ()

let (original_args, autocomplete) =
  (* for shell aliases *)
  let rec move_autocomplete_token_upfront acc = function
    | "bash_autocomplete" :: prev_arg :: cur_arg :: script :: args ->
        let args = List.rev acc @ args in
        (args, Some (prev_arg, cur_arg, script))
    | x :: rest ->
        move_autocomplete_token_upfront (x :: acc) rest
    | [] ->
        (List.rev acc, None)
  in
  match Array.to_list Sys.argv with
  | _ :: args ->
      move_autocomplete_token_upfront [] args
  | [] ->
      ([], None)

let () =
    ignore
    Clic.(
      setup_formatter
        Format.std_formatter
        (if Unix.isatty Unix.stdout then Ansi else Plain)
        Short) ;
  let result =
    Lwt_main.run
      ( Clic.parse_global_options Global_options.options () original_args
        >>=? fun ((say_hello, say_something), args) ->
      match autocomplete with
      | Some (prev_arg, cur_arg, script) ->
          Clic.autocompletion
            ~script
            ~cur_arg
            ~prev_arg
            ~args:original_args
            ~global_options:Global_options.options
            commands_with_man
            ()
          >>=? fun completions ->
          List.iter print_endline completions ;
          return_unit
      | None -> (
        handle_global_options say_hello say_something ;
        match args with
        | [] ->
            return_unit
        | _ ->
            Clic.dispatch commands_with_man () args ) )
  in
  match result with
  | Ok global_options ->
      global_options
  | Error [Clic.Help command] ->
      Clic.usage
        Format.std_formatter
        ~executable_name:(Filename.basename Sys.executable_name)
        ~global_options:Global_options.options
        (match command with None -> [] | Some c -> [c]) ;
      exit 0
  | Error errors ->
      Clic.pp_cli_errors
        Format.err_formatter
        ~executable_name:(Filename.basename Sys.executable_name)
        ~global_options:Global_options.options
        ~default:(fun fmt err -> Error_monad.pp_print_error fmt [err])
        errors ;
      exit 1
